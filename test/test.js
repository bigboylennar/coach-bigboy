const memberTask = require('../public/tasks/memberTasks')
const trainorTask = require('../public/tasks/trainorTasks')
const { expect } = require('chai')

describe('task', () => {
    describe('.getGrossSalary', () => {
        it('gets the gross salary of a trainor', () => {
            const students = 5
            const baseSalary = 10000
            const additional = 250
            const actual = trainorTask.getGrossSalary(baseSalary, additional, students)
            expect(actual).to.equal(11250)
        })
    })
    describe('.getExpirationDate', () => {
        it('gets the expiration date of membership', () => {
            const enrollDate = new Date('2017-10-30T03:17:50.367Z')
            const expireDate = new Date('2017-11-30T03:17:50.367Z')
            const actual = memberTask.getExpirationDate(enrollDate)
            expect(actual.getTime()).to.equal(expireDate.getTime())
        })
    })
})