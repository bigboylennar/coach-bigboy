const express = require("express")
const path = require("path")
const bodyParser = require("body-parser")
const MongoClient = require("mongodb").MongoClient
const ObjectId = require("mongodb").ObjectId
const url = "mongodb://localhost:27017/coachBigboy"
const secret = "lodipetmalu"

const memberTaskHandler = require('./taskHandlers/memberTaskHandler')
const trainorTaskHandler = require('./taskHandlers/trainorTaskHandler')
const searchTaskHandler = require('./taskHandlers/searchTaskHandler')
const programTaskHandler = require('./taskHandlers/programTaskHandler')
const userTaskHandler = require('./taskHandlers/userTaskHandler')
const authentication = require('./taskHandlers/authentication')

const app = express()

app
  .use(express.static(path.join(process.cwd(), "public")))
  .use(bodyParser.json())
  .use(bodyParser.urlencoded({ extended: true }))
  .use(authentication.authenticate)

  memberTaskHandler.handleMemberTask(app)
  trainorTaskHandler.handleTrainorTask(app)
  searchTaskHandler.handleSearchTask(app)
  programTaskHandler.handleProgramTasks(app)
  userTaskHandler.handleUserTask(app)

MongoClient.connect(url, (error, database) => {
  if (error) {
    console.log(error.message)
  }
  const port = 8084
  db = database
  app.listen(port, () => {
    console.log(`http://localhost:${port}`)
  })
})