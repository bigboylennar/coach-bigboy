const ObjectId = require("mongodb").ObjectId
const validate = require("./validate")

function addtrainorToProgram(trainor, response) {
  db
    .collection("programs")
    .findAndModify(
      { name: trainor.program },
      [],
      { $push: { trainors: ObjectId(trainor._id) } },
      { upsert: true },
      (err, result) => {
        if (err) {
          console.log(err.message)
        } else {
          response.json({ message: "saved" })
        }
      }
    )
}

function saveTrainor(trainor, response) {
  db
    .collection("trainors")
    .update({ _id: trainor._id }, trainor, { upsert: true }, (err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        addtrainorToProgram(trainor, response)
      }
    })
}

function deleteTrainor(trainor, response) {
  db.collection("trainors").deleteOne({ _id: trainor._id }, (err, result) => {
    if (err) {
      console.log(err.message)
    } else {
      response.json({ message: "deleted" })
    }
  })
}

function removeTrainorFromProgram(trainor, response, action) {
  db
    .collection("programs")
    .updateOne(
      { trainors: { $elemMatch: { $eq: ObjectId(trainor._id) } } },
      { $pull: { trainors: ObjectId(trainor._id) } },
      (err, result) => {
        if (err) {
          console.log(err.message)
        } else {
          if (action === "save") {
            saveTrainor(trainor, response)
          } else {
            removeTrainorFromStudent(trainor, response)
          }
        }
      }
    )
}

function removeTrainorFromStudent(trainor, response) {
  db
    .collection("members")
    .update(
      { trainor: ObjectId(trainor._id) },
      { $set: { trainor: "", trainorName: "" } },
      (err, result) => {
        if (err) {
          console.log(err.message)
        } else {
          deleteTrainor(trainor, response)
        }
      }
    )
}

getTrainor = (request, response) => {
  const id = request.params.trainorId
  db.collection("trainors").findOne({ _id: ObjectId(id) }, (err, result) => {
    if (err) {
      console.log(err.message)
    } else {
      response.json(result)
    }
  })
}

getTrainorByProgram = (request, response) => {
  const trainingProgram = request.params.program
  const query = { program: trainingProgram }

  db
    .collection("trainors")
    .find(query)
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

getAllTrainor = (request, response) => {
  db
    .collection("trainors")
    .find()
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

upsertDeleteTrainor = (request, response) => {
  const trainor = request.body
  const action = request.params.action
  if (validate.trainorData(trainor) || action === "delete") {
    trainor._id = trainor._id ? ObjectId(trainor._id) : new ObjectId()
    removeTrainorFromProgram(trainor, response, action)
  } else {
    response.statusCode = 400
    response.json({ message: "Invalid Data" })
  }
}

handleTrainorTask = (app) => {
  app.post("/trainor/:action", (request, response) => {
    upsertDeleteTrainor(request, response)
   })
   
   app.get("/all/trainors", (request, response) => {
     getAllTrainor(request, response)
   })
   
   
   app.get("/:program/trainors", (request, response) => {
     getTrainorByProgram(request, response)
   })
   
   app.get("/trainor/:trainorId", (request, response) => {
     getTrainor(request, response)
   })
}

module.exports.handleTrainorTask = handleTrainorTask