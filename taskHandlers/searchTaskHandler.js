allData = (request, response) => {
  const program = request.params.program
  const collection = request.params.collection
  const search = request.params.searchTerm
  const exp = new RegExp(`^${search}`, "i")
  let query = {
    $or: [{ firstName: { $regex: exp } }, { lastName: { $regex: exp } }],
    program: program
  }
  if (program === "all") {
    query = {
      $or: [{ firstName: { $regex: exp } }, { lastName: { $regex: exp } }]
    }
  }
  db
    .collection(collection)
    .find(query)
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

expired = (request, response) => {
  const search = request.params.searchTerm
  const now = new Date()
  const exp = new RegExp(`^${search}`, "i")
  const query = {
    $or: [{ firstName: { $regex: exp } }, { lastName: { $regex: exp } }],
    expireDate: { $lte: now.toJSON() }
  }
  db
    .collection("members")
    .find(query)
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

handleSearchTask = (app) => {
  app.get("/:program/:collection/:searchTerm", (request, response) => {
    allData(request, response)
  })

  app.get("/members/expired/:searchTerm", (request, response) => {
    expired(request, response)
  })
}

module.exports.handleSearchTask = handleSearchTask