getAllProgram = (request, response) => {
  db
    .collection("programs")
    .find()
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

handleProgramTasks = (app) => {
  app.get("/all-programs", (request, response) => {
    getAllProgram(request, response)
  })
}

module.exports.handleProgramTasks = handleProgramTasks