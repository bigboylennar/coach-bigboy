const jwt = require("jsonwebtoken")

function extractJWT(authHeader) {
  return authHeader.slice(7)
}

module.exports.authenticate = (request, response, next) => {
  const path = request.path
  if (path === "/login" || path === "/signup" || path === "/favicon.ico") {
    next()
  }
  else {
    const secret = "lodipetmalu"
    const authHeader = request.header("Authorization")

    jwt.verify(extractJWT(authHeader), secret, (err, decoded) => {
      if (err) {
        response.statusCode = 403
        response.json({ message: "access denied" })
      } else {
        next()
      }
    })
  }
}
