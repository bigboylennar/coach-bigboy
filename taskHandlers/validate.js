module.exports.trainorData = (trainor) => {
    if (typeof trainor !== 'object') {
      return false
    }
    else {
      let checks = {
        firstName : false,
        lastName : false,
        baseSalary : false,
        additional : false,
        program : false
      }
      const {firstName, lastName, baseSalary, additional, program} = trainor
      if (typeof firstName === 'string' && firstName.length > 0 && firstName.match(/[A-Za-z\s]/)) {
        checks.firstName = true
      }
      if (typeof lastName === 'string' && lastName.length > 0 && lastName.match(/[A-Za-z\s]/)) {
        checks.lastName = true
      }
      if (typeof baseSalary === 'string' && (1 * baseSalary) >= 0 && baseSalary.match(/\d/)) {
        checks.baseSalary = true
      }
      if (typeof additional === 'string' && (1 * additional) >= 0 && additional.match(/\d/)) {
        checks.additional = true
      }
      if (typeof program === 'string' && program.length > 0 && 
        ['plyo', 'hiit', 'metafit', 'weights'].includes(program) && program.match(/[A-Za-z]/)) {
        checks.program = true
      }
      return Object.values(checks).every(item => item === true)
    }
  }
  
module.exports.memberData = (member) => {
    if (typeof member !== 'object') {
      return false
    }
    else {
      let checks = {
        firstName : false,
        lastName : false,
        program : false,
        trainor : false,
        trainorName : false,
        expireDate : false
      }
      const {firstName, lastName, program, trainor, trainorName, expireDate} = member
      if (typeof firstName === 'string' && firstName.length > 0 && firstName.match(/[A-Za-z\s]/)) {
        checks.firstName = true
      }
      if (typeof lastName === 'string' && lastName.length > 0 && lastName.match(/[A-Za-z\s]/)) {
        checks.lastName = true
      }
      if (typeof program === 'string' && program.length > 0 && 
        ['plyo', 'hiit', 'metafit', 'weights'].includes(program) && program.match(/[A-Za-z]/)) {
        checks.program = true
      }
      if (typeof trainor === 'string' && trainor.length === 24) {
        checks.trainor = true
      }
      if (typeof trainorName === 'string' && trainorName.length > 0 && trainorName.match(/[A-Za-z\s]/)) {
        checks.trainorName = true
      }
      if (typeof expireDate === 'string' && new Date(expireDate) instanceof Date) {
        checks.expireDate = true
      }
  
      return Object.values(checks).every(item => item === true)
    }
  }
  
module.exports.date = (member) => {
    if (typeof member !== 'object') {
      return false
    }
    else {
      const {expireDate} = member
      if (typeof expireDate === 'string' && new Date(expireDate) instanceof Date) {
        return true
      }
      return false
    }
  }
  
module.exports.userData = (user) => {
    if (typeof user !== 'object') {
      return false
    }
    else {
      const {username} = user
      if (typeof username === 'string' && username.length > 0 && username.match(/[A-Za-z0-9]/)) {
        return true
      }
      return false
    }
  }