const ObjectId = require("mongodb").ObjectId
const validate = require("./validate")

function addStudentToTrainor(member, response) {
  db
    .collection("trainors")
    .update(
      { _id: member.trainor },
      { $push: { students: ObjectId(member._id) } },
      (err, result) => {
        if (err) {
          console.log(err.message)
        } else {
          addStudentToProgram(member, response)
        }
      }
    )
}

function addStudentToProgram(member, response) {
  db
    .collection("programs")
    .findAndModify(
      { name: member.program },
      [],
      { $push: { students: ObjectId(member._id) } },
      { upsert: true },
      (err, result) => {
        if (err) {
          console.log(err.message)
        } else {
          response.json({ message: "saved" })
        }
      }
    )
}

function saveMember(member, response) {
  db
    .collection("members")
    .update({ _id: member._id }, member, { upsert: true }, (err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        addStudentToTrainor(member, response)
      }
    })
}

function deleteMember(member, response) {
  db.collection("members").deleteOne({ _id: member._id }, (err, result) => {
    if (err) {
      console.log(err.message)
    } else {
      response.json({ message: "deleted" })
    }
  })
}

function removeStudentFromTrainor(member, response, action) {
  db
    .collection("trainors")
    .updateOne(
      { students: { $elemMatch: { $eq: ObjectId(member._id) } } },
      { $pull: { students: ObjectId(member._id) } },
      (err, result) => {
        if (err) {
          console.log(err.message)
        } else {
          if (action === "expired") {
            response.json({ message: "expired" })
          } else {
            removeStudentFromProgram(member, response, action)
          }
        }
      }
    )
}

function removeStudentFromProgram(member, response, action) {
  db
    .collection("programs")
    .updateOne(
      { students: { $elemMatch: { $eq: ObjectId(member._id) } } },
      { $pull: { students: ObjectId(member._id) } },
      (err, result) => {
        if (err) {
          console.log(err.message)
        } else {
          if (action === "save") {
            saveMember(member, response)
          } else {
            deleteMember(member, response)
          }
        }
      }
    )
}

getMemberByProgram = (request, response) => {
  const trainingProgram = request.params.program
  const query = { program: trainingProgram }
  db
    .collection("members")
    .find(query)
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

getAllMember = (request, response) => {
  db
    .collection("members")
    .find()
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

upsertDeleteMember = (request, response) => {
  const member = request.body
  const action = request.params.action
  if (validate.memberData(member) || action === "delete") {
    member.trainor = ObjectId(member.trainor)
    member._id = member._id ? ObjectId(member._id) : new ObjectId()
    removeStudentFromTrainor(member, response, action)
  } else {
    response.statusCode = 400
    response.json({ message: "Invalid Data" })
  }
}

renewMember = (request, response) => {
  const member = request.body
  if (validate.date(member)) {
    db.collection("members").updateOne({
      _id: ObjectId(member._id)
    },
    { $set: { expireDate: member.expireDate } },
    (err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json({ message: "renewed" })
      }
    })
  } else {
    response.statusCode = 400
    response.json({ message: "Invalid Data" })
  }
}

getExpiredMember = (request, response) => {
  const now = new Date()
  db
    .collection("members")
    .find({ expireDate: { $lte: now.toJSON() } })
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

getMember = (request, response) => {
  const id = request.params.memberId
  db.collection("members").findOne({ _id: ObjectId(id) }, (err, result) => {
    if (err) {
      console.log(err.message)
    } else {
      response.json(result)
    }
  })
}

handleMemberTask = app => {
  app.post("/member/:action", (request, response) => {
    upsertDeleteMember(request, response)
  })

  app.post("/member-renew", (request, response) => {
    renewMember(request, response)
  })

  app.get("/members/expired", (request, response) => {
    getExpiredMember(request, response)
  })

  app.get("/all/members", (request, response) => {
    getAllMember(request, response)
  })

  app.get("/:program/members", (request, response) => {
    getMemberByProgram(request, response)
  })

  app.get("/member/:memberId", (request, response) => {
    getMember(request, response)
  })
}

module.exports.handleMemberTask = handleMemberTask