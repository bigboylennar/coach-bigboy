const bcrypt = require("bcryptjs")
const jwt =  require("jsonwebtoken")
const ObjectId = require("mongodb").ObjectId

const validate = require("./validate")

getUser = (request, response) => {
  console.log(request.params.username)
  db
    .collection("users")
    .find({ username: request.params.username })
    .toArray((err, result) => {
      if (err) {
        console.log(err.message)
      } else {
        response.json(result)
      }
    })
}

signup = (request, response) => {
  const user = request.body
  if (validate.userData(user)) {
    bcrypt.genSalt(10, (error, salt) => {
      bcrypt.hash(user.password, salt, (error, hashPassword) => {
        user._id = user._id ? ObjectId(user._id) : new ObjectId()
        user.password = hashPassword
        db
          .collection("users")
          .update({ _id: user._id }, user, { upsert: true }, (err, result) => {
            if (err) {
              console.log(err.message)
            } else {
              response.json({ message: "signed up" })
            }
          })
      })
    })
  } else {
    response.statusCode = 400
    response.json({ message: "Invalid Data" })
  }
}

login = (request, response) => {
  const user = request.body
  if (validate.userData(user)) {
    db
      .collection("users")
      .find({ username: user.username })
      .toArray((err, result) => {
        if (result.length === 1) {
          bcrypt.compare(user.password, result[0].password, (err, result) => {
            if (result) {
              jwt.sign({ username: user.username }, secret, (error, token) => {
                response.json({ jwtToken: token, message: "log in success" })
              })
            }
          })
        } else {
          response.statusCode = 401
          response.json({ message: "invalid username or password" })
        }
      })
  } else {
    response.statusCode = 400
    response.json({ message: "Invalid Data" })
  }
}

handleUserTask = app => {
  app.post("/signup", (request, response) => {
    signup(request, response)
  })

  app.post("/login", (request, response) => {
    login(request, response)
  })

  app.get("/user/:username", (request, response) => {
    getUser(request, response)
  })
}

module.exports.handleUserTask = handleUserTask