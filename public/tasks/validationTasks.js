function checkTextField(divId, isNumber) {
  const textField = $(`#${divId}`)[0]
  textField.valid = true
  if (textField.value === "") {
    changeStyleToError(divId, "Field must not be blank")
  } else {
    if (!isNumber && textField.value.match(/[^A-Za-z\s]/)) {
      changeStyleToError(divId, "Field only accepts letters")
    } else if (isNumber && textField.value.match(/\D/)) {
      changeStyleToError(divId, "Field only accepts numbers")
    } else if (isNumber && textField.value * 1 < 0) {
      changeStyleToError(divId, "Field only accepts positive numbers")
    }
  }

  if (textField.valid === true) {
    changeStyleToNormal(divId)
  }
}

function checkTrainor() {
  const trainingProgram = $("input[name=programs]:checked")
  if (trainingProgram[0]) {
    $("#programTrainorAlert")[0].innerText = ""
    $("#programTrainorAlert")[0].valid = true
  } else {
    $("#programTrainorAlert")[0].valid = false
    $("#programTrainorAlert")[0].innerText = "Please select program first"
  }
}

function changeStyleToError(divId, message) {
  $(`#${divId}`)[0].valid = false
  $(`#${divId}`)[0].style.borderColor = "red"
  $(`#${divId}Alert`)[0].innerText = message
}

function changeStyleToNormal(divId) {
  $(`#${divId}`)[0].style.borderColor = "#ccc"
  $(`#${divId}Alert`)[0].innerText = ""
}

function verifySignUpInfo() {
  //validate username
  const usernameField = $("#username")[0]
  if (usernameField.value === "") {
    changeStyleToError("username", "Field must not be empty")
  } else if (usernameField.value.match(/[^A-Za-z0-9]/)) {
    changeStyleToError("username", "Invalid input. Expects letters or numbers")
  } else {
    usernameField.valid = true
    changeStyleToNormal("username")
  }

  //validate password
  if ($("#password")[0].value === "") {
    changeStyleToError("password", "Field must not be empty")
  } else if ($("#password")[0].value !== $("#confirmPassword")[0].value) {
    changeStyleToError("password", "Password doesn't match")
  } else {
    $("#password")[0].valid = true
    changeStyleToNormal("password")
  }

  const inputs = [$("#username")[0].valid, $("#password")[0].valid]
  if (Object.values(inputs).every(item => item === true)) {
    signUser()
  }
}

function changeStudentInputToNormal() {
  const textFields = ["firstNameStudent", "lastNameStudent"]
  for (textField of textFields) {
    changeStyleToNormal(textField)
  }
  $("#trainorAlert")[0].innerText = ""
  $("#programStudentAlert")[0].innerText = ""
}

function changeTrainorInputToNormal() {
  const textFields = [
    "firstNameTrainor",
    "lastNameTrainor",
    "baseSalary",
    "additional"
  ]
  for (textField of textFields) {
    changeStyleToNormal(textField)
  }
  $("#programTrainorAlert")[0].innerText = ""
}

function setToken(response) {
  localStorage.setItem("token", response.jwtToken)
}
