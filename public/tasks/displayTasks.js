function displayPanels(result) {
  $('#panels')[0].innerHTML = ''
  for (const row of result) {
    const member = row.students ? row.students.length : 0
    const trainor = row.trainors ? row.trainors.length : 0
    $('#panels')[0].innerHTML += `
      <div class="col-sm-3">
        <div class="panel panel-default">
          <div class="panel-heading">${row.name}</div>
          <div class="panel-body">
            <div class="col-flex">
              <div>
                <h3>${member} Members</h3>
              </div>
              <div class="row-flex">
                <a href="#table" onclick="displayMemberByProgram('${row.name}')">
                <span class="glyphicon glyphicon-list"></span> Show Members</a>
              </div>
              </br>
              <div>
                <h3>${trainor} Trainors</h3>
              </div>
              <div class="row-flex">
                <a href="#table" onclick="displayTrainorByProgram('${row.name}')">
                <span class="glyphicon glyphicon-list"></span> Show Trainors</a>
              </div>
            </div>
          </div>
        </div>
      </div>`
  }
}

function showPanels() {
  get('/all-programs', displayPanels)
}

function clearTable() {
  $('#heading')[0].innerText = ''
  $('#table')[0].innerHTML = $('#tableTemplate')[0].innerHTML
}

function deleteTrainor(trainorId) {
  const data = {
    _id : trainorId
  }
  post('/trainor/delete', displayAlert, showPanels, data)
  clearTable()
}

function displayAlert(response) {
  const message = response.message
  const alertClass = message === 'deleted' ? 'danger' : 'success'
  $('#alert-container')[0].innerHTML = `
  <div class="alert alert-${alertClass}">
    <strong>${message}!</strong>
  </div>`
  removeAlert()
}

function removeAlert() {
  const func = setTimeout(() => {
    $('#alert-container')[0].innerHTML = ''
  }, 3000)
  return func
}

function displayMainView() {
  $('#main-body')[0].innerHTML = $('#main')[0].innerHTML
  get('/all-programs', displayPanels)
}

function displaySignupLoginView() {
  $('#main-body')[0].innerHTML = $('#signlog')[0].innerHTML
  showLogInForm()
}

function showSignUpForm() {
  $("#form")[0].innerHTML = $("#signupTemplate")[0].innerHTML
}

function showLogInForm() {
  $("#form")[0].innerHTML = $("#loginTemplate")[0].innerHTML
}