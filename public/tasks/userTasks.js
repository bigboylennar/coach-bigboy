function signUser() {
  const data = {
    username: $("#username")[0].value,
    password: $("#password")[0].value
  }
  post("/signup", displayAlert, showLogInForm, data)
}

function loginUser() {
  const data = {
    username: $("#username")[0].value,
    password: $("#password")[0].value
  }
  post("/login", setToken, displayMainView, data)
}
