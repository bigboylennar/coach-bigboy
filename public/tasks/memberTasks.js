function saveStudent() {
  const trainingProgram = $("input[name=programs]:checked")
  const date = new Date()
  const data = {
    _id: $("#member-id")[0].value,
    firstName: $("#firstNameStudent")[0].value,
    lastName: $("#lastNameStudent")[0].value,
    program: trainingProgram[0].value,
    trainor: $("#trainor-id")[0].value,
    trainorName: $("#selected-trainor")[0].innerText,
    expireDate: getExpirationDate(date)
  }
  post("/member/save", displayAlert, showPanels, data)
  emptyEnrollModal()
  clearTable()
}

function fillMemberTable(result) {
  $("#table-header")[0].innerHTML = `
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Program</th>
      <th>Membership Expiration</th>
    </tr>`
  $("#table-content")[0].innerHTML = ""
  for (const row of result) {
    const now = new Date()
    const expireDate = new Date(row.expireDate)
    const expired = now.getTime() > expireDate.getTime()
    const trClass = expired ? "expired" : ""
    const action =
      trClass === "expired"
        ? `
      <a onclick="renewMember('${row._id}')">
      <span class="glyphicon glyphicon-retweet"></span></a>`
        : `
      <a data-toggle="modal" href="#enrollModal" onclick="editMemberInfo('${row._id}')">
      <span class="glyphicon glyphicon-pencil"></span></a>`

    $("#table-content")[0].innerHTML += `
          <tr class="${trClass}">
            <td>${row.firstName}</td>
            <td>${row.lastName}</td>
            <td>${row.program}</td>
            <td>${expireDate.toDateString()}</td>
            <td>
            ${action}
            <a onclick="deleteMember('${row._id}')">
            <span class="glyphicon glyphicon-trash"></span></a>
            </td>
          </tr>`
  }
}

function setTrainorId(id) {
  $("#trainor-id")[0].value = id
}

function setId(id, name) {
  displaySelectedTrainor(name)
  setTrainorId(id)
}

function displaySelectedTrainor(name) {
  $("#selected-trainor")[0].innerText = name
  $("#trainor-id")[0].valid = true
  $("#trainorAlert")[0].innerText = ""
}

function displayMemberByProgram(program) {
  $("#table")[0].innerHTML = ""
  $("#heading")[0].attributes.program = program
  $("#heading")[0].attributes.collection = "members"
  $("#heading")[0].innerText = program + " members"
  $("#table")[0].innerHTML += $("#tableTemplate")[0].innerHTML
  const url = `${program}/members`
  get(url, fillMemberTable)
}

function selectTrainor() {
  const trainingProgram = $("input[name=programs]:checked")
  if (trainingProgram[0]) {
    $("#trainor-list")[0].innerHTML = ""
    $("#programStudentAlert")[0].innerText = ""
    const url = `/${trainingProgram[0].value}/trainors`
    get(url, getTrainorSelection)
  } else {
    $("#programStudentAlert")[0].innerText = "Please select program first"
  }
}

function deleteMember(memberId) {
  const data = {
    _id: memberId
  }
  post("/member/delete", displayAlert, showPanels, data)
  clearTable()
}

function renewMember(memberId) {
  const date = new Date()
  const data = {
    _id: memberId,
    expireDate: getExpirationDate(date)
  }

  post("/member-renew", displayAlert, showPanels, data)
  clearTable()
}

function editMemberInfo(memberId) {
  const url = `/member/${memberId}`
  get(url, fillEnrollModal)
}

function getTrainorSelection(result) {
  for (const row of result) {
    const name = `${row.firstName} ${row.lastName}`
    $("#trainor-list")[0].innerHTML += `
        <li><a href="#" onclick="setId('${row._id}', '${name}')">
          ${name}
        </a></li>`
  }
}

function emptyEnrollModal() {
  $("#firstNameStudent")[0].value = ""
  $("#lastNameStudent")[0].value = ""
  $("#trainor-id")[0].value = ""
  $("#selected-trainor")[0].innerText = "No Trainor Selected"
}

getExpirationDate = date => {
  const expireDate = date
  expireDate.setMonth(expireDate.getMonth() + 1)

  return expireDate
}

module.exports.getExpirationDate = getExpirationDate
