function fillTrainorTable(result) {
  $("#table-header")[0].innerHTML = `
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
      <th>Program</th>
      <th>No. of Students</th>
      <th>Gross Salary</th>
    </tr>`
  $("#table-content")[0].innerHTML = ""
  for (const row of result) {
    const students = row.students ? row.students.length : 0
    const salary = getGrossSalary(row.baseSalary, row.additional, students)
    const gross = salary ? salary : 0
    $("#table-content")[0].innerHTML += `
          <tr>
            <td>${row.firstName}</td>
            <td>${row.lastName}</td>
            <td>${row.program}</td>
            <td>${students}</td>
            <td>&#8369 ${gross}</td>
            <td>
            <a data-toggle="modal" href="#addTrainorModal" onclick="editTrainorInfo('${row._id}')">
            <span class="glyphicon glyphicon-pencil"></span></a>
            <a onclick="deleteTrainor('${row._id}')">
            <span class="glyphicon glyphicon-trash"></span></a>
            </td>
          </tr>`
  }
}

function fillEnrollModal(result) {
  $("#firstNameStudent")[0].value = result.firstName
  $("#lastNameStudent")[0].value = result.lastName
  $("#member-id")[0].value = result._id
  $("#trainor-id")[0].value = result.trainor
  $("#selected-trainor")[0].innerText = result.trainorName
}

function emptyTrainorModal() {
  $("#firstNameTrainor")[0].value = ""
  $("#lastNameTrainor")[0].value = ""
  $("#baseSalary")[0].value = ""
  $("#additional")[0].value = ""
}

function fillTrainorModal(result) {
  $("#firstNameTrainor")[0].value = result.firstName
  $("#lastNameTrainor")[0].value = result.lastName
  $("#trainor")[0].value = result._id
  $("#baseSalary")[0].value = result.baseSalary
  $("#additional")[0].value = result.additional
}

function displayTrainorByProgram(program) {
  $("#table")[0].innerHTML = ""
  $("#heading")[0].innerText = program + " trainors"
  $("#heading")[0].attributes.program = program
  $("#heading")[0].attributes.collection = "trainors"
  $("#table")[0].innerHTML += $("#tableTemplate")[0].innerHTML
  const url = `${program}/trainors`
  get(url, fillTrainorTable)
}

function editTrainorInfo(trainorId) {
  const url = `/trainor/${trainorId}`
  get(url, fillTrainorModal)
}

function saveTrainor() {
  const trainingProgram = $("input[name=programs]:checked")
  const data = {
    _id: $("#trainor")[0].value,
    firstName: $("#firstNameTrainor")[0].value,
    lastName: $("#lastNameTrainor")[0].value,
    baseSalary: $("#baseSalary")[0].value,
    additional: $("#additional")[0].value,
    program: trainingProgram[0].value,
    students: []
  }
  post("/trainor/save", displayAlert, showPanels, data)
  emptyTrainorModal()
  clearTable()
}

getGrossSalary = (baseSalary, additional, students) => {
  return 1 * baseSalary + students * additional
}

module.exports.getGrossSalary = getGrossSalary
