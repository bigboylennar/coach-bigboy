function get (url, task) {
    ajax('GET', url, task, null, null)
}

function post(url, task, display, data) {
    ajax('POST', url, task, display, data)
}

function ajax(method, url, task, display, data) {
    const xhr = new XMLHttpRequest()
    xhr.onreadystatechange = () => {
        if(xhr.readyState === 4 && xhr.status === 200) {
            if(task) {
                task(JSON.parse(xhr.responseText))
            }
            if (display) {
                display()
            }
        }
    }
    xhr.open(method, url, true)
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.setRequestHeader("Authorization", `Bearer ${window.localStorage.getItem('token')}`)
    if (method === 'POST' && data) {
        xhr.send(JSON.stringify(data))
    }
    else {
        xhr.send()
    }
}

