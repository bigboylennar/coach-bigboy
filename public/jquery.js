$(document).ready(() => {

  if (localStorage.token) {
    displayMainView()
  } else {
    displaySignupLoginView()
  }

  $(document).on('click', '#all-members', () => {
    $("#heading")[0].innerText = "All Members"
    $("#heading")[0].attributes.program = "all"
    $("#heading")[0].attributes.collection = "members"
    $("#table")[0].innerHTML = $("#tableTemplate")[0].innerHTML
    get("/all/members", fillMemberTable)
  })

  $(document).on('click', '#enrollBtn', () => {
    const textFields = {
      firstNameStudent: false,
      lastNameStudent: false
    }
    for (const [key, value] of Object.entries(textFields)) {
      checkTextField(key, value)
    }
    if ($("#trainor-id")[0].value === "") {
      $("#trainor-id")[0].valid = false
      $("#trainorAlert")[0].innerText = "Please select trainor"
    }

    const inputs = [
      $("#firstNameStudent")[0].valid,
      $("#lastNameStudent")[0].valid,
      $("#trainor-id")[0].valid
    ]
    if (Object.values(inputs).every(item => item === true)) {
      saveStudent()
    }
  })

  $(document).on('click', '#addTrainorBtn', () => {
    const textFields = {
      firstNameTrainor: false,
      lastNameTrainor: false,
      baseSalary: true,
      additional: true
    }
    for (const [key, value] of Object.entries(textFields)) {
      checkTextField(key, value)
    }
    checkTrainor()

    const inputs = [
      $("#firstNameTrainor")[0].valid,
      $("#lastNameTrainor")[0].valid,
      $("#baseSalary")[0].valid,
      $("#additional")[0].valid,
      $("#programTrainorAlert")[0].valid
    ]
    if (Object.values(inputs).every(item => item === true)) {
      saveTrainor()
    }
  })

  $(document).on('hide.bs.modal', '#enrollModal', () => {
    changeStudentInputToNormal()
  })

  $(document).on('hide.bs.modal', '#addTrainorModal', () => {
    changeTrainorInputToNormal()
  })

  $(document).on('click', '#all-trainors', () => {
    $("#heading")[0].innerText = "All Trainors"
    $("#heading")[0].attributes.program = "all"
    $("#heading")[0].attributes.collection = "trainors"
    $("#table")[0].innerHTML = $("#tableTemplate")[0].innerHTML
    get("/all/trainors", fillTrainorTable)
  })

  $(document).on('click', '#expired-members', () => {
    $("#heading")[0].innerText = "Expired Members"
    $("#heading")[0].attributes.collection = "expired"
    $("#table")[0].innerHTML = $("#tableTemplate")[0].innerHTML
    get("/members/expired", fillMemberTable)
  })

  $(document).on('click', '.close', () => {
    emptyEnrollModal()
    emptyTrainorModal()
  })

  $(document).on('click', '#logout', () => {
    localStorage.setItem("token", "")
    displaySignupLoginView()
  })

  $(document).on('keyup', '#searchBar', () => {
    const searchTerm = $("#searchBar")[0].value
    const collection = $("#heading")[0].attributes.collection
    const program = $("#heading")[0].attributes.program
    $("#table")[0].innerHTML = $("#tableTemplate")[0].innerHTML
    if (collection !== "expired") {
      const url = `${program}/${collection}/${searchTerm}`
      const task =
        collection === "trainors" ? fillTrainorTable : fillMemberTable
      get(url, task)
    } else {
      get(`/members/expired/${searchTerm}`, fillMemberTable)
    }
  })


})
